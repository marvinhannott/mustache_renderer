import 'package:mustache_renderer/mustache_renderer.dart';

void main() {
  final renderer = MustacheRenderer('./example/public')
    ..registerAllSync(['index', 'header']);

  final values = {
    'header_message': 'stupid old header',
    'title': 'hello world',
    'list': [
      {'element': 'first'},
      {'element': 'second'}
    ]
  };

  // Send rendered page to Client
  final rendered = renderer.render('index', values);
  print(rendered);
}
