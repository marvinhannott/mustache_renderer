import 'dart:io';

import 'package:mustache_template/mustache.dart';

class MustacheRenderer {
  final Map<String, Template> _cache = {};
  final String viewsPath;
  final String templateExtension;
  final String namespaceDelimiter;

  /// [viewsPath] points to the directory in which all views / templates for this renderer can be found.
  ///
  /// [templateExtension] is the extension you use for your templates. It has to start with '.' (dot).
  ///
  /// [namespaceDelimiter] delimits directory hirachies within [viewsPath]. A template `example.html` in a subdirectory
  /// `sub` would be called `sub.example`. This way namespace collisions can be avoided.
  /// To apply the template `example.html` as a partial, this partial also has to be called `{{> sub.example}}`.
  MustacheRenderer(this.viewsPath,
      {this.templateExtension = '.html', this.namespaceDelimiter = '.'});

  Template _partialResolver(String name) {
    return _cache[name];
  }

  String _filePath(String templateName) {
    final resolvedName = templateName.replaceAll(namespaceDelimiter, '/');
    return '$viewsPath/$resolvedName$templateExtension';
  }

  /// Registers a template, pre-parses it, and finally caches the result.
  /// [name] is the name of the template file without the extension,
  /// so `example.html` would be called `example`.
  ///
  /// [lenient] decides wether the renderer renders in lenient or strict mode.
  /// [htmlEscape] decides wether all HTML is escaped or not.
  Future<void> register(String name,
      {bool lenient = false, bool htmlEscape = true}) async {
    final templString = await File(_filePath(name)).readAsString();
    _cache[name] = Template(templString,
        partialResolver: _partialResolver,
        htmlEscapeValues: htmlEscape,
        lenient: lenient);
  }

  /// Same as [register], but synchronous.
  void registerSync(String name,
      {bool lenient = false, bool htmlEscape = true}) {
    final templString = File(_filePath(name)).readAsStringSync();
    _cache[name] = Template(templString,
        partialResolver: _partialResolver,
        htmlEscapeValues: htmlEscape,
        lenient: lenient);
  }

  /// Calls [register] for all [names].
  /// [lenient] and [htmlEscape] are used on every template.
  Future<void> registerAll(List<String> names,
      {bool lenient = false, bool htmlEscape = true}) {
    return Future.forEach(names,
        (name) => register(name, lenient: lenient, htmlEscape: htmlEscape));
  }

  /// Same as [registerAll], but synchronous.
  void registerAllSync(List<String> names,
      {bool lenient = false, bool htmlEscape = true}) {
    for (final name in names) {
      registerSync(name, lenient: lenient, htmlEscape: htmlEscape);
    }
  }

  /// Returns the rendered view.
  String render(String name, [dynamic values = const {}]) {
    return _cache[name].renderString(values);
  }

  /// Does the same as [render], but writes the result to [sink].
  void renderTo(String name, StringSink sink, [dynamic values = const {}]) {
    _cache[name].render(values, sink);
  }
}
